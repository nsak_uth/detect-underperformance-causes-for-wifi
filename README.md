A supervised machine learning model for accurately detecting 5 well known 802.11 pathologies by making use of
four popular classification algorithms: Decision Trees, Random Forests, Support
Vector Machines and K-Nearest Neighbors.

The data were gathered from modeling the pathologies in NiTos Testbed. About 1000 samples were collected and cleaned for every class(wifi pathology).

After the initial split of the data in training and testing sets, the classifiers were optimized by cross-validation.
The training set was fed to a greedy algorithm(Exhaustive Grid Search)alongside a set of hyper-parameters for each 
estimator(CVC model, RandomForest model etc). The greedy algorithm runs the estimators for each set of predefined 
hyper-parameters with 5-folds cross-validation in order to find the most efficient parameter options in terms of 
precision, recall and accuracy. The algorithm returns the set of the parameters options that will later be used
in the final validation of the models with the test set. 

More details about the experiments, the pathologies and the model can be found to the related paper "On the Employment of Machine Learning
Techniques for Troubleshooting WiFi Networks" that will be published on IEEE CCNC 2019
