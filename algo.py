# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 15:40:15 2017

@author: nsak
"""
from __future__ import print_function
from sklearn import tree
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cluster import KMeans
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_recall_fscore_support
import matplotlib.pyplot as plt
import numpy as np
import csv, os, random, time
import itertools
#param estimation function

from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report

'''
#################################################
#### Decision Trees                          ####
#################################################
'''
def decision_tree_classifier(X,Y,X_test,Y_test):
    """
    DESCRIPTION:
    INPUT : X--> list of lists [[0, 0], [1, 1], ..]
            Y--> list with labels
            X_test --> like X
            Y_test --> like Y
    RETURN: pred, accuraccy           
    """
    print("################# DT CLASSIFIER #################")
    clf = tree.DecisionTreeClassifier(criterion='entropy', max_depth= 2000, max_features=3, max_leaf_nodes=2000, min_samples_split=4, splitter='random')
    acc = []
    prec = []
    rec = []
    f = []
    for n in range(5):
        clf = clf.fit(X, Y)
        pred = clf.predict(X_test)
        accuracy = accuracy_score(Y_test,pred)
        prec.append(precision_recall_fscore_support(Y_test, pred, average='weighted')[0])
        rec.append(precision_recall_fscore_support(Y_test, pred, average='weighted')[1])
        f.append(precision_recall_fscore_support(Y_test, pred, average='weighted')[2])
        acc.append(accuracy)
    avg_acc = sum(acc)/len(acc)
    avg_prec = sum(prec)/len(prec)
    avg_rec = sum(rec)/len(rec)
    avg_f = sum(f)/len(f)
    print("Decision trees accuracy is: ",accuracy)
    print_precision_recall(Y_test,pred)
    print()
    print("Average accuracy %0.3f, precision %0.3f, recall %0.3f, f_score: %0.3f" %(avg_acc,avg_prec,avg_rec,avg_f) )
    return pred
'''
#################################################
#### k Neirest Neighnors                     ####
#################################################
'''
def knn_classifier(X,Y,X_test,Y_test):

    from sklearn.neighbors import KNeighborsClassifier
    print("################# KNN CLASSIFIER #################")
    clf = KNeighborsClassifier(algorithm='auto', leaf_size=3, n_jobs= 1, n_neighbors=3, p= 1)
    acc = []
    prec = []
    rec = []
    f = []
    for n in range(5):
        clf.fit(np.asarray(X),np.asarray(Y) )
        pred = clf.predict(X_test)
        accuracy = accuracy_score(Y_test,pred)
        prec.append(precision_recall_fscore_support(Y_test, pred, average='weighted')[0])
        rec.append(precision_recall_fscore_support(Y_test, pred, average='weighted')[1])
        f.append(precision_recall_fscore_support(Y_test, pred, average='weighted')[2])
        acc.append(accuracy)
    avg_acc = sum(acc)/len(acc)
    avg_prec = sum(prec)/len(prec)
    avg_rec = sum(rec)/len(rec)
    avg_f = sum(f)/len(f)
    print("KNN accuracy is: ",accuracy)
    print_precision_recall(Y_test,pred)
    print()
    print("Average accuracy %0.3f, precision %0.3f, recall %0.3f, f_score: %0.3f" %(avg_acc,avg_prec,avg_rec,avg_f) ) 
    return pred
'''
#################################################
#### Naive Bayes                             ####
#################################################
'''     
def naive_bayes_classifier(X,Y,X_test,Y_test):
    """
    DESCRIPTION:
    INPUT : X--> numpy (list of lists [[0, 0], [1, 1], ..] )
            Y--> numpy( list with labels)
            X_test --> like X
            Y_test --> like Y
    RETURN: pred, accuraccy    
    """
    #X = np.array([[-1, -1], [-2, -1], [-3, -2], [1, 1], [2, 1], [3, 2]])
    #Y = np.array([1, 1, 1, 2, 2, 2])

    clf = GaussianNB()
    clf.fit(X, Y)
    pred = clf.predict(X_test)
    accuracy = accuracy_score(Y_test,pred)
    print("Naive Bayes accuracy is: ",accuracy)
    print_precision_recall(Y_test,pred)
    return pred
    
'''
#################################################
#### Random Forest                           ####
#################################################
''' 
def random_forests_classifier(X,Y,X_test,Y_test):
    """
    DESCRIPTION:
    INPUT : X--> list of lists [[0, 0], [1, 1], ..]
            Y--> list with labels
            X_test --> like X
            Y_test --> like Y
    RETURN: pred, accuraccy           
    """
    print("################# RF CLASSIFIER #################")
    clf = RandomForestClassifier(criterion='entropy', max_features='auto', n_estimators=30)
    acc = []
    prec = []
    rec = []
    f = []
    for n in range(5):
        clf = clf.fit(X, Y)
        pred = clf.predict(X_test)
        accuracy = accuracy_score(Y_test,pred)
        prec.append(precision_recall_fscore_support(Y_test, pred, average='weighted')[0])
        rec.append(precision_recall_fscore_support(Y_test, pred, average='weighted')[1])
        f.append(precision_recall_fscore_support(Y_test, pred, average='weighted')[2])
        acc.append(accuracy)
    avg_acc = sum(acc)/len(acc)
    avg_prec = sum(prec)/len(prec)
    avg_rec = sum(rec)/len(rec)
    avg_f = sum(f)/len(f)
    print("Random Forest accuracy is: ",accuracy)
    print_precision_recall(Y_test,pred)
    print()
    print("Average accuracy %0.3f, precision %0.3f, recall %0.3f, f_score: %0.3f" %(avg_acc,avg_prec,avg_rec,avg_f) )  
    return pred
        


'''
#################################################
#### Support Vector Machines                 ####
#################################################
''' 
def svm_classifier(X,Y,X_test,Y_test):
    """
    DESCRIPTION:
    INPUT : X--> numpy (list of lists [[0, 0], [1, 1], ..] )
            Y--> numpy( list with labels)
            X_test --> like X
            Y_test --> like Y
    RETURN: pred, accuraccy    
    """
    print("################# SVM CLASSIFIER #################")
    clf = svm.SVC(C=10, gamma='auto', kernel='rbf')#'linear')
    acc = []
    prec = []
    rec = []
    f = []
    for n in range(5):
        svm_start = time.time()
        clf.fit(X, Y)
        svm_end = time.time()
        pred = clf.predict(X_test)
        accuracy = accuracy_score(Y_test,pred)
        
        #print('Plithos tou X',len(X))
        #print('Plithos tou X_test', len(X_test))
        
        prec.append(precision_recall_fscore_support(Y_test, pred, average='weighted')[0])
        rec.append(precision_recall_fscore_support(Y_test, pred, average='weighted')[1])
        f.append(precision_recall_fscore_support(Y_test, pred, average='weighted')[2])
        acc.append(accuracy)
    avg_acc = sum(acc)/len(acc)
    avg_prec = sum(prec)/len(prec)
    avg_rec = sum(rec)/len(rec)
    avg_f = sum(f)/len(f)
    print("SVC linear kernel accuracy is: ",accuracy)
    print_precision_recall(Y_test,pred)
    print()
    print("Average accuracy %0.3f, precision %0.3f, recall %0.3f, f_score: %0.3f" %(avg_acc,avg_prec,avg_rec,avg_f) )  
    return pred

#  print 'apotelesma',clf.predict([[92.905, 95.7657]])
'''
#################################################
#### K-Means Clustering                      ####
#################################################
''' 
def kmeans_clustering(x_elements):
    """ 
    DESCRIPTION: kmeans for clustering of unlabeled data(unsupervised learning)
    INPUT:  X--> numpy (list of lists [[0, 0], [1, 1], ..] )
    RETURN: Y--> numpy( list with labels) #mallon auto
    """
    X = np.array(x_elements)
    #kmeans = KMeans(n_clusters=3, random_state=0).fit(X)
    #print "kmeans labels: ",kmeans.labels_#array([0, 0, 0, 1, 1, 1], dtype=int32)
    #print "kmeans cluster centers: ",kmeans.cluster_centers_#array([[ 1.,  2.],[ 4.,  2.]])
    y_pred = KMeans(n_clusters=6, random_state=0).fit_predict(X)
    return y_pred

'''
#################################################
#### Helper functions to read the data from csv ##
#################################################
''' 
def read_from_csv(csv_file):
  with open(csv_file) as csvfile:
    labeled = []	
    reader = csv.DictReader(csvfile)
    for row in reader:
        try:
            labeled.append([float(row['PDR']),float(row['Average Attempts']),int(row['LABEL']),int(row['Modulation'])])
        except Exception as e:
            print(row,'exception:')
            print(e)
            print(csv_file)

  return labeled

def read_from_csv8(csv_file):
  """
  Reads our data from csv file that contains
  all 7 modulations(PDR-Attemts) on the same line
  """
  with open(csv_file) as csvfile:
    labeled = []
    reader = csv.reader(csvfile,delimiter=',')
    for row in reader:
        try:
            #print row[1:18]
            #print row[1],row[2],r
            labeled.append(row[1:18])#[float(row[1]),float(row[2]),float(row[3]),float(row[4])])
        except Exception as e:
            print(row,'exception:')
            print(e)
            print (csv_file)
  labeled.pop(0) #remove column descriptions
  return labeled

'''
#################################################
#### Tuning the hyper-parameters             ####
#################################################
'''
def param_est(estimator,tuned_parameters,X_train, y_train, X_test,  y_test):
 
    print(__doc__)

    # Set the parameters by cross-validation
    #tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
    #                    'C': [1, 10, 100, 1000]},
    #                    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]

    scores = ['precision_macro', 'recall_macro','accuracy']
    with open("3d_model_results.txt","a") as f:
        for score in scores:
            print("# Tuning hyper-parameters for %s" % score)
            f.write("# Tuning hyper-parameters for %s \n" % score)
            print()
            if estimator == 'svc':
                clf = GridSearchCV(svm.SVC(), tuned_parameters, cv=5,
                                scoring=score)
            elif estimator == 'rf':
                clf = GridSearchCV(RandomForestClassifier(), tuned_parameters, cv=5,
                                scoring=score)
            elif estimator == 'dt':
                clf = GridSearchCV(tree.DecisionTreeClassifier(), tuned_parameters, cv=5,
                                scoring=score)
            elif estimator == 'knn':
                clf = GridSearchCV(KNeighborsClassifier(), tuned_parameters, cv=5,
                                scoring=score)
            clf.fit(X_train, y_train)
            print("Best parameters set found on development set:")
            f.write("Best parameters set found on development set:\n")
            print()
            print(clf.best_params_)
            f.write(str(clf.best_params_))
            print()
            print("Grid scores on development set:")
            f.write("Grid scores on development set:\n")
            print()
            means = clf.cv_results_['mean_test_score']
            stds = clf.cv_results_['std_test_score']
            for mean, std, params in zip(means, stds, clf.cv_results_['params']):
                print("%0.3f (+/-%0.03f) for %r"
                    % (mean, std * 2, params))
                f.write("%0.3f (+/-%0.03f) for %r"% (mean, std * 2, params))
            print()
    
            print("Detailed classification report:")
            f.write("Detailed classification report:\n")
            print()
            print("The model is trained on the full development set.")
            print("The scores are computed on the full evaluation set.")
            print()
            y_true, y_pred = y_test, clf.predict(X_test)
            print(classification_report(y_true, y_pred))
            f.write(str(classification_report(y_true, y_pred)))
            print()
            print("Accuracy Score")
            print()
            print(accuracy_score(y_true, y_pred))
            f.write("Accuracy : "+str(accuracy_score(y_true, y_pred)) + "\n")
            print()
            f.flush()

'''
#################################################
#### Get and handle the data                 ####
#################################################
''' 
def preprocessing(labels=[],vector='default'):
    """
    DESCRIPTION:
    INPUT : 
    RETURN: x,y,x_test,y_test           
    """

    script_dir = os.path.dirname(__file__)

    rel_path1 = "Experiment Data/data_3_D_vector/experiments_1.csv"
    path_label1 = os.path.join(script_dir,rel_path1)

    rel_path2 = "Experiment Data/data_3_D_vector/experiments_2.csv"
    path_label2 = os.path.join(script_dir,rel_path2)

    #diafora sto 3 peirama epeidh htan standar oi komvoi kai ta upoloipa ektws apo rate kai BW einai oti ta pernw ola mazi,test kai mh test
    #rel_path3 = "experiment3/experiments.csv"  
    #path_label3 = os.path.join(script_dir,rel_path3)

    rel_path4 = "Experiment Data/data_3_D_vector/experiments_4.csv" #exp4_capture/experiments.csv"
    path_label4 = os.path.join(script_dir,rel_path4)

    rel_path5 = "Experiment Data/data_3_D_vector/experiments_5_hidden.csv"  #exp4_hidden/experiments.csv"
    path_label5 = os.path.join(script_dir,rel_path5)

    rel_path6 = "Experiment Data/data_3_D_vector/experiments_6_oven.csv"
    path_label6 = os.path.join(script_dir,rel_path6)
    '''
    #################################################
    #### Append the data to the right structures ####
    #################################################
    '''
    exp1_data = read_from_csv(path_label1)
    random.shuffle(exp1_data)
    exp1_data = exp1_data
    label1 = exp1_data[int(len(exp1_data)*0.0):int(len(exp1_data)*0.8)]
    label1_test = exp1_data[int(len(exp1_data)*0.8):int(len(exp1_data)*1.0)]
    print("class 1 length ",len(label1))
    print("class 1_test length ", len(label1_test))

    exp2_data = read_from_csv(path_label2)
    random.shuffle(exp2_data)
    exp2_data = exp2_data
    label2 = exp2_data[int(len(exp2_data)*0.0):int(len(exp2_data)*0.8)]
    label2_test = exp2_data[int(len(exp2_data)*0.8):int(len(exp2_data)*1.0)]
    print("class 2 length ",len(label2))
    print("class 2_test length ", len(label2_test))
    """
    exp3_data = read_from_csv(path_label3)
    random.shuffle(exp3_data)
    exp3_data = exp3_data
    label3 = exp3_data[int(len(exp3_data)*0.0):int(len(exp3_data)*0.8)]
    label3_test = exp3_data[int(len(exp3_data)*0.8):int(len(exp3_data)*1.0)]
    print "class 3 length ",len(label3)
    print "class 3_test length ", len(label3_test)
    """
    exp4_data = read_from_csv(path_label4) #capture
    random.shuffle(exp4_data)
    exp4_data = exp4_data #for 3d data [:1100] /default
    label4 = exp4_data[int(len(exp4_data)*0.0):int(len(exp4_data)*0.8)]
    label4_test = exp4_data[int(len(exp4_data)*0.8):int(len(exp4_data)*1.0)]
    print("class 4 length ",len(label4))
    print("class 4_test length ", len(label4_test))

    exp5_data = read_from_csv(path_label5) #hidden
    random.shuffle(exp5_data)
    exp5_data = exp5_data#for 3d data [:1100] / default
    label5 = exp5_data[int(len(exp5_data)*0.0):int(len(exp5_data)*0.8)]
    label5_test = exp5_data[int(len(exp5_data)*0.8):int(len(exp5_data)*1.0)]
    print("class 5 length ",len(label5))
    print("class 5_test length ", len(label5_test))


    exp6_data = read_from_csv(path_label6)
    random.shuffle(exp6_data)
    exp6_data = exp6_data
    label6 = exp6_data[int(len(exp6_data)*0.0):int(len(exp6_data)*0.8)]
    label6_test = exp6_data[int(len(exp6_data)*0.8):int(len(exp6_data)*1.0)]
    print("class 6 length ",len(label6))
    print("class 6_test length ", len(label6_test))

    #print ("class 1 and 2 length",len(read_from_csv(rel_path1) + read_from_csv(rel_path2)))
    '''
    #################################################
    #### Concatenate our data in one list        ####
    #################################################
    '''
    classes = {1: [label1, label1_test], 2: [label2, label2_test],  4: [label4, label4_test],
               5: [label5, label5_test], 6: [label6, label6_test]} #3: [label3, label3_test],

    if len(labels) > 0:
        labeled_data,test_data = [], []
        for label in labels:
            labeled_data += classes[label][0]
            test_data += classes[label][1]
    else:
        labeled_data = label1 + label2 +  label4 + label5 + label6  #label3 + read_from_csv(rel_path1)+ read_from_csv(rel_path2)
        test_data = label1_test + label2_test  +  label4_test + label5_test +label6_test #label3_test +read_from_csv(path_test1) +read_from_csv(path_test2)
    
    '''
    #################################################
    #### shuffling our data                      ####
    #################################################
    '''    
    random.shuffle(labeled_data)
    random.shuffle(test_data)
    
    '''
    #################################################
    #### Splitting our data in X and Y lists and np
    #### arrays for correct format input for 
    #### classifiers
    #################################################    
    '''
    y = []
    x = []
    y_test = []
    x_test = []
    if vector == 'default': #pdr,attempts,modulation,label
        for i in labeled_data:
            y.append(i[2])
            x.append([i[0],i[1],i[3]])  #0=PDR ,1=Average Attempts ,3=modulation

        for i in test_data:
            y_test.append(i[2])
            x_test.append([i[0],i[1],i[3]])
    else:                   # vector is 7 modulations of PDR,Attempts
        for i in labeled_data:
            print(i)
            y.append(i[16])
            x.append(i[:17])  # 0=PDR ,1=Average Attempts ,3=modulation

        for i in test_data:
            y_test.append(i[16])
            x_test.append(i[:17])

   
    return x,y,x_test,y_test
'''
#################################################
#### Plot Confusion matrix                   ####
#################################################
'''
def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.3f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    
'''
#################################################
#### Print precision, recall, f1-score       #### 
#################################################
''' 
def print_precision_recall(y_test,y_pred):
    print("(macro)   Precision %f, Recall %f, fscore %f , %s" %precision_recall_fscore_support(y_test, y_pred, average='macro') )
    print("(micro)   Precision %f, Recall %f, fscore %f , %s" %precision_recall_fscore_support(y_test, y_pred, average='micro') )
    print("(weighted)Precision %f, Recall %f, fscore %f , %s" %precision_recall_fscore_support(y_test, y_pred, average='weighted') )

'''
#################################################
#### Convert our (X,Y) data to numpy array   #### 
#### format                                  ####
#################################################
''' 
def convert_to_np_array(x,y,x_test,y_test):
    X = np.array(x)
    Y = np.array(y)
    X_test = np.array(x_test)
    Y_test = np.array(y_test)
    
    return X,Y,X_test,Y_test

'''
#################################################
#### Scatter Plot our data                   ####
#################################################
'''
def plot_classes(X,Y,title,x_label='X',y_label='Y'):
    class1 = []
    class2 = []
    #class3 = []
    class4 = []
    class5 = []
    class6 = []
    for x,y in zip(X,Y):
        #print set(Y)
        if y == 1:
            class1.append(x)
        if y == 2:
            class2.append(x)
        #if y == 3:
        #    class3.append(x)
        if y == 4:
            class4.append(x)
        if y == 5:
            class5.append(x)
        if y == 6:
            class6.append(x)
            #ckass6y.append(y)
    class1 = np.array(class1)
    class2 = np.array(class2)
    #class3 = np.array(class3)
    class4 = np.array(class4)
    class5 = np.array(class5)
    class6 = np.array(class6) 
 
   
    colors = ['b', 'c', 'm', 'r','g'] #'y',
    plt.figure(figsize=(14, 14),dpi = 100)
    cl1 = plt.scatter(class1[:,0], class1[:,1], marker='x', color=colors[0],s=80)
    cl2 = plt.scatter(class2[:,0], class2[:,1], marker='+', color=colors[1],s=80)
#    if len(class3)>0:
#        cl3 = plt.scatter(class3[:,0], class3[:,1], marker='x', color=colors[2],s=80)
#    else:
#        cl3 = None
    cl4 = plt.scatter(class4[:,0], class4[:,1], marker='*', color=colors[3],s=80)
    if len(class5)>0:
        cl5 = plt.scatter(class5[:,0], class5[:,1], marker='x', color=colors[4],s=80)
    else:
        cl5 = None
    if len(class6)>0:
        cl6 = plt.scatter(class6[:,0], class6[:,1], marker='o', color=colors[2],s=80)
    else:
        cl6 = None
    plt.legend((cl1, cl2,cl4,cl5,cl6),
           ('Low-SNR', 'Contention',  'Capture', 'Hidden', 'Oven'),
           scatterpoints=1,
           loc='lower left',
           ncol=3,
           fontsize=8) #cl3,'Hidden2',
           
    plt.title(title)
    plt.xlabel(x_label, fontsize=15)
    plt.ylabel(y_label, fontsize=16)
    
    plt.savefig(title.split(' ')[0]+".png")
    #plt.show()
    #return class1,class2,class3,class4,class5,class6
       
       
def plot_classes_old(X,Y,title,x_label='X',y_label='Y'):
    plt.figure(figsize=(14, 14),dpi = 100)
    #plt.subplot(221)
    #colors = ['red','green','blue']
    plt.scatter(X[:, 0], X[:, 1], c=Y, s=100)
    plt.title(title)
    plt.xlabel(x_label, fontsize=15)
    plt.ylabel(y_label, fontsize=16)
    
    plt.savefig(title.split(' ')[0]+".png")
    #plt.show()


if __name__ == '__main__':
    
    x,y,x_test,y_test = preprocessing()#labels=[4,5,6]) #,vector='eights')
    X,Y,X_test,Y_test = convert_to_np_array(x,y,x_test,y_test)
    
    '''
    #################################################
    #### Parameter Tuning     ####
    #################################################
    '''
    """
    print("##################################")
    print("#######Parameter Estimation#######")
          
    #print("================= SVC parameter tuning===========================")      
    with open("3d_model_results.txt","a") as f:
        f.write("================= SVC parameter tuning===========================\n")
             
    tuned_parameters =[{'kernel': ['rbf'], 'gamma': ['auto',1e-3, 1e-4],
                     'C': [1, 10, 100, 1000],'random_state':[2]}]
    param_est('svc',tuned_parameters,X,Y,X_test,Y_test)
    
    ####################### Random Forest ##################################
    with open("3d_model_results.txt","a") as f:
        f.write("####################### Random Forest #############################\n")
    tuned_parameters = [{'n_estimators':[5,10,20,30,40],'criterion': ['gini','entropy'],
                     'max_features':['auto','sqrt','log2',2,3],'random_state':[2]}]
    param_est('rf',tuned_parameters,X,Y,X_test,Y_test)
    
    ###################### Decision Trees #################################
    print("================= DT parameter tuning===========================")
    with open("3d_model_results.txt","a") as f:
        f.write("================= DT parameter tuning===========================\n")
        
    tuned_parameters = [{'criterion':['gini','entropy'],'splitter': ['best','random'],
                    'min_samples_split':[2,3,4,10],'min_samples_leaf':[1,2,3,4,6,8],
                    'max_features':['auto','sqrt','log2',2,3]},
                    
                    {'criterion': ['gini','entropy'], 'splitter': ['best','random'],
                     'max_depth':[1000,1500,2000], 
                     'max_leaf_nodes': [40,100, 200, 300, 1000,1500,2000],
                     'max_features':['auto','sqrt','log2',2,3],
                     'min_samples_split':[2,3,4,10]},
                    
                   {'criterion': ['gini','entropy'], 'splitter': ['best','random'],
                     'max_features':['auto','sqrt','log2',2,3],
                     'min_samples_split':[2,3,4,10]}]
    param_est('dt',tuned_parameters,X,Y,X_test,Y_test)

    ###################### KNN ############################################
    print("================= KNN parameter tuning =========================")
    with open("3d_model_results.txt","a") as f:
        f.write("================= KNN parameter tuning =========================\n")

    tuned_parameters = [{'n_neighbors':[3,5,7],'weights':['uniform','distance'],'algorithm': ['auto','brute'],
                     'p': [1, 2],'n_jobs': [2]},
                    {'n_neighbors':[3,5,7],'algorithm': ['ball_tree','kd_tree'],
                     'weights':['uniform','distance'],'leaf_size': [3, 10, 30, 100],
                    'p': [1, 2],'n_jobs': [2]}]
    
    param_est('knn',tuned_parameters,X,Y,X_test,Y_test)
    """
    
    print("########## Accuracy, Precision, Recall, F ###################")
 
    knn_start = time.time()
    knn_pred = knn_classifier(x,y,x_test,y_test)
    knn_end = time.time()

    dt_start = time.time()
    dt_pred = decision_tree_classifier(x,y,x_test,y_test)
    dt_end = time.time()
    
    rf_start = time.time()
    rf_pred = random_forests_classifier(x,y,x_test,y_test)
    rf_end = time.time()
    #convert to numpy arrays
    

    nb_start = time.time()
    nb_pred = naive_bayes_classifier(X,Y,X_test,Y_test)
    nb_end = time.time()

    svm_start = time.time()
    svm_pred = svm_classifier(X,Y,X_test,Y_test)
    svm_end = time.time()

    km_start = time.time()
    km_pred = kmeans_clustering(X)
    km_end = time.time()

    #Elapsed times of each algorithm
    dt_elapsed = dt_end - dt_start
    rf_elapsed = rf_end - rf_start
    nb_elapsed = nb_end - nb_start
    svm_elapsed = svm_end - svm_start
    km_elapsed = km_end - km_start
    knn_elapsed = knn_end - knn_start

    print("##################################")
    print("Decision tree elapsed ",dt_elapsed)
    print("KNN elapsed ",knn_elapsed)
    print("Random Forest elapsed ",rf_elapsed)
    print("Naive Bayes elapsed ",nb_elapsed)
    print("SVM elapsed ", svm_elapsed)
    print("K-Means elapsed ", km_elapsed)


    
    #------------------Plots---------------------
    """
    plot_classes(X, Y, "Train set","PDR",'Average Attempts')
    plot_classes(X_test, Y_test, "Test set original Labels", "PDR", 'Average Attempts')

    plot_classes(X_test, dt_pred, "Decision Tree Classifier\n Prediction", "PDR", 'Average Attempts')
    plot_classes(X_test, rf_pred, "Random Forests Classifier\n Prediction", "PDR", 'Average Attempts')
    plot_classes(X_test, nb_pred, "Naive Bayes Classifier\n Prediction", "PDR", 'Average Attempts')
    plot_classes(X_test, svm_pred, "Support Vector Machines Classifier\n Prediction", "PDR", 'Average Attempts')
    plot_classes(X, km_pred, "K Means Clustering")
    """
    #plot_classes_old(X, km_pred, "K Means Clustering","PDR", 'Average Attempts')
    #plot_classes(x, y, "Train set","PDR",'Average Attempts')
    #plot_classes(x_test, y_test, "Test set original Labels", "PDR", 'Average Attempts')
    #plot_classes(x_test, dt_pred.tolist(), "Decision Tree Classifier\n Prediction", "PDR", 'Average Attempts')
    #plot_classes(x_test, knn_pred.tolist(), "KNN Classifier\n Prediction", "PDR", 'Average Attempts')
    #plot_classes(x_test, rf_pred.tolist(), "Random Forests Classifier\n Prediction", "PDR", 'Average Attempts')
    #plot_classes(x_test, svm_pred.tolist(), "Support Vector Machines Classifier\n Prediction", "PDR", 'Average Attempts')
    #plot_classes(x_test, nb_pred.tolist(), "Naive Bayes Classifier\n Prediction", "PDR", 'Average Attempts')


    

    '''
    #################################################
    #### Testing classes with each other         ####
    #################################################
    '''
    #x, y, x_test, y_test = preprocessing()#labels=[1,5])

    #X, Y, X_test, Y_test = convert_to_np_array(x, y, x_test, y_test)
    #svm_pred2 = svm_classifier(X,Y,X_test,Y_test)
    #accuracy = accuracy_score(Y_test,svm_pred2)
    #print "SVC linear kernel accuracy is: ",accuracy
    #plot_classes(X_test, Y_test, "Test set original Labels", "PDR", 'Average Attempts')
    #plot_classes(X_test, svm_pred2, "Support Vector Machines Classifier\n Prediction", "PDR", 'Average Attempts')

    '''
    #################################################
    ####          Confusion matrix               ####
    #################################################
    '''
    # Compute confusion matrix SVM
    cnf_matrix = confusion_matrix(y_test, svm_pred)
    np.set_printoptions(precision=2)

    class_names = ['l-snr','contention','capture','hidden','oven'] #'hidden2',
    # Plot non-normalized confusion matrix
    plt.figure()
    plot_confusion_matrix(cnf_matrix, classes=class_names,
                          title='Confusion matrix, without normalization-svm')

    # Plot normalized confusion matrix
    plt.figure()
    plot_confusion_matrix(cnf_matrix, classes=class_names, normalize=True,
                          title='Normalized confusion matrix-svm')
                          
    #---------------------------RANDOM FOREST-----------------------------------
    # Compute confusion matrix
    cnf_matrix = confusion_matrix(y_test, rf_pred)
    np.set_printoptions(precision=2)

    class_names = ['l-snr','contention','capture','hidden','oven'] #'hidden2',
    # Plot non-normalized confusion matrix
    plt.figure()
    plot_confusion_matrix(cnf_matrix, classes=class_names,
                          title='Confusion matrix, without normalization-rf')

    # Plot normalized confusion matrix
    plt.figure()
    plot_confusion_matrix(cnf_matrix, classes=class_names, normalize=True,
                          title='Normalized confusion matrix-rf')
  #---------------------------KNN-----------------------------------
    # Compute confusion matrix
    cnf_matrix = confusion_matrix(y_test, knn_pred)
    np.set_printoptions(precision=2)

    class_names = ['l-snr','contention','capture','hidden','oven'] #'hidden2',
    # Plot non-normalized confusion matrix
    plt.figure()
    plot_confusion_matrix(cnf_matrix, classes=class_names,
                          title='Confusion matrix, without normalization-knn')

    # Plot normalized confusion matrix
    plt.figure()
    plot_confusion_matrix(cnf_matrix, classes=class_names, normalize=True,
                          title='Normalized confusion matrix-knn')

    plt.show()

# To activate this environment in anaconda, use:
# > activate py27
#
# To deactivate an active environment, use:
# > deactivate
#
# * for power-users using bash, you must source
#