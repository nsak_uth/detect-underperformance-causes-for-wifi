import csv
import os


def write_to_csv(keraies,power,peirama,pdrs,attempts,nodes='91_92',ssir='32/70',label='1'):
	with open('experiments.csv' ,'ab') as csvfile:
		experiments = csv.writer(csvfile, delimiter=',',quotechar='|',quoting=csv.QUOTE_MINIMAL)
		#experiments.writerow(['#Keraiwn,Power,#peiramatos,PDR,Average Attempts,Nodes,ssir,LABEL,end'])
		for i in range(len(modulation)):
			experiments.writerow([keraies+','+power+','+peirama+','+pdrs[i]+','+attempts[i]+','+nodes+','+'32/70,'+label+', '])

	with open('experiments.csv') as csvfile:
		reader = csv.DictReader(csvfile)
		for row in reader:
			print row['LABEL']
def create_csv():
	with open('experiments.csv' ,'wb') as csvfile:
		experiments = csv.writer(csvfile, delimiter=',',quotechar='|',quoting=csv.QUOTE_MINIMAL)
		experiments.writerow(['#Keraiwn,Power,#peiramatos,PDR,Average Attempts,Nodes,ssir,LABEL,end'])	
def read_txt(exp_name):
	with open(exp_name,'r') as f: #exp_1_1_4_9.txt
		lines = f.readlines()
		modulation = []
		pdrs = []
		attempts = []
		count = 0
		for line in lines:
		# 1h grammh modulation,2o sleep, 3o PDr, 4o attempts
		        if count % 4 == 0: 
		                modulation.append(line[0].strip('\n'))
		        if count % 4 == 1: 
		                pass
		        if count % 4 == 2:
		                pdrs.append(line.strip('\n'))
			
		        if count % 4 == 3:
		                attempts.append(line.strip('\n'))
		        count += 1
	return modulation,pdrs,attempts


txts = [filename for filename in os.listdir('.') if filename.startswith("exp_")]
create_csv()
for txt in txts:
	modulation,pdrs,attempts = read_txt(txt)
	write_to_csv(txt[4:7],txt[8],txt[10],pdrs,attempts)





